####
# IMPORTATIONS
####

from Monte_Carlo import *
import numpy as np

####
# FONCTIONS
####


def UCB(noeud, n_iterations):
    """Input : noeud dont il faut évaluer le potentiel
               n étapes de l'algorithme
    Return : UCB = Rm(n)/Nm(n) + sqrt(log(n)/Nm(n)) avec
             Rm(n) = score noeud m à l'étape n
             Nm(n) = nombre de sélections du noeud m à l'étape n
    """
    return (noeud.score/(noeud.nb_visite+1e-8)) + np.sqrt(np.log(noeud.parent.nb_visite+1e-8)/(noeud.nb_visite+1e-8))
