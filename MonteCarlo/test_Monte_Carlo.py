####
# IMPORTATIONS
####

from Monte_Carlo import *

####
# TESTS
####


def test_genere_enfant():
    parent = {"gene": {
        "AA": [35.62, 7.2],
        "AC": [34.4, 1.1],
        "AG": [27.7, 8.4],
        "AT": [31.5, 2.6]}, "parent": None, "score": 300, "next": 4, "nb_visite": 3}
    e1 = genere_enfant(parent, 0, 0)
    e2 = {"gene": {
        "AA": [35.62, 7.2],
        "AC": [34.4, 1.10],
        "AG": [27.7, 8.4],
        "AT": [31.5, 2.6],
        "CA": [34.5-0.9, 3.5-34]}, "parent": parent, "score": 300, "next": 5, "nb_visite": 0}
    # print(e1)
    assert e1 == e2


# test_genere_enfant()
