####
# IMPORTATIONS
####

import numpy as np
from RotTable import RotTable
from Traj3D import Traj3D
import numpy.linalg as npl

####
# CONSTANTES
####

table = __ORIGINAL_ROT_TABLE = {
    "AA": [35.62, 7.2, -154,      0.06,  0.6, 0],
    "AC": [34.4, 1.1,  143,      1.3,  5, 0],
    "AG": [27.7, 8.4,    2,      1.5,  3, 0],
    "AT": [31.5, 2.6,    0,      1.1,  2, 0],
    "CA": [34.5, 3.5,  -64,      0.9, 34, 0],
    "CC": [33.67, 2.1,  -57,      0.07,  2.1, 0],
    "CG": [29.8, 6.7,    0,      1.1,  1.5, 0],
    "CT": [27.7, 8.4,   -2,      1.5,  3, 0],
    "GA": [36.9, 5.3,  120,      0.9,  6, 0],
    "GC": [40, 5,  180,      1.2,  1.275, 0],
    "GG": [33.67, 2.1,   57,      0.07,  2.1, 0],
    "GT": [34.4, 1.1, -143,      1.3,  5, 0],
    "TA": [36, 0.9,    0,      1.1,  2, 0],
    "TC": [36.9, 5.3, -120,      0.9,  6, 0],
    "TG": [34.5, 3.5,   64,      0.9, 34, 0],
    "TT": [35.62, 7.2,  154,      0.06,  0.6, 0]
}

####
# FONCTIONS ET CLASSES
####


def transform(gene, chaine):
    """
    Les gènes sont codés par des dictionnaires dont les clées ont pour valeurs des listes de taille 2 (on ne considère pas la variance)
    La fonction transform permet de réintroduire les variances afin de manipuler des objet de classe RotTable 
    Input:gêne aléatoire et chaîne dont il est issu
    Return:matrice de rotation liée à ce nouveau gêne aléatoire
    """
    dic = table.copy()
    res = {}
    n = len(chaine)
    for i in range(1, n):
        d1, d2 = chaine[i-1], chaine[i]
        doublet = d1+d2
        res[doublet] = [gene[doublet][0], gene[doublet][1], dic[doublet]
                        [2], dic[doublet][3], dic[doublet][4], dic[doublet][5]]
    for x in table:
        if x not in res:
            res[x] = [0, 0]
    return res


class Individual():
    """
    Défini ce qu'est un individu pour l'algorithme génétique 
    Attributs : Des gènes (sous forme de dictionnaire de table de rotation), une chaine ADN, un score (fitness), un indicateur booléen qui 
    indique si fitness a déja été calculée 
    Méthodes: random_dico, get_gene, get_chaine, fitness, fit

    """
    # Récupération des 3 paramètres des doublets dans doublets
    # Récupération des 3 écart-types associés dans ecart_types
    doublets = {key: table[key][:3] for key in table}
    ecart_types = {key: table[key][3:] for key in table}

    def random_dico(self):
        """
        Génère des gènes de façon aléatoire dans les intervalles autorisés et en respectant les symétries
        Input: //
        Return: matrice de rotation aléatoire avec les valeurs et écarts-type de la table
        """
        dic = table.copy()
        res = {}
        # Pour les doublets qui n'ont pas de symétrique
        for doublet in ["AT", "CG", "TA", "GC"]:
            mini = Individual.doublets[doublet][0] - \
                Individual.ecart_types[doublet][0]
            maxi = Individual.doublets[doublet][0] + \
                Individual.ecart_types[doublet][0]
            a = np.random.uniform(mini, maxi)
            mini = Individual.doublets[doublet][1] - \
                Individual.ecart_types[doublet][1]
            maxi = Individual.doublets[doublet][1] + \
                Individual.ecart_types[doublet][1]
            b = np.random.uniform(mini, maxi)
            res[doublet] = [a, b, dic[doublet][2], dic[doublet]
                            [3], dic[doublet][4], dic[doublet][5]]

        # Pour les doublets symétriques
        for duodoublet in [["AA", "TT"], ["AC", "GT"], ["AG", "CT"], ["CA", "TG"], ["CC", "GG"], ["GA", "TC"]]:
            doublet = duodoublet[0]
            mini = Individual.doublets[doublet][0] - \
                Individual.ecart_types[doublet][0]
            maxi = Individual.doublets[doublet][0] + \
                Individual.ecart_types[doublet][0]
            a = np.random.uniform(mini, maxi)
            mini = Individual.doublets[doublet][1] - \
                Individual.ecart_types[doublet][1]
            maxi = Individual.doublets[doublet][1] + \
                Individual.ecart_types[doublet][1]
            b = np.random.uniform(mini, maxi)
            res[doublet] = [a, b, dic[doublet][2], dic[doublet]
                            [3], dic[doublet][4], dic[doublet][5]]
            res[duodoublet[1]] = [a, b, -dic[doublet][2],
                                  dic[doublet][3], dic[doublet][4], dic[doublet][5]]
        return res

    def __init__(self, chaine):
        self.gene = self.random_dico()
        self.chaine = chaine
        self.fitn = 0
        self.fitnessbool = 0

    def get_gene(self):
        return self.gene

    def get_chaine(self):
        return self.chaine

    def fitness(self, gene, chaine):
        """
        Permet d'attribuer un score aux individus
        Input:gêne aléatoire et chaîne dont il est issu
        Return: fitness calculée sur ce gêne défini par la distance euclidienne entre les extrémités
        """
        new_rot_table = RotTable(
            gene)  # Table de rotation particulière à ce gêne
        traj = Traj3D()
        traj.compute(chaine, new_rot_table)
        rot_table = new_rot_table.get_table()

        # Trajet sous la forme [Vect(a,b,c,1),...,Vect(a,b,c,1)]
        path = traj.getTraj()

        # On définit les points importants qui vont servir à définir la distance et l'orientation
        first = np.asarray(path[0])
        last = np.asarray(path[-1])
        second = np.asarray(path[1])
        before_last = np.asarray(path[-2])
        res = np.linalg.norm(last-first)
        dinucleotide = chaine[-1]+chaine[0]
        second = np.asarray(path[1])
        # En pratique, on observe cette distance entre deux nucléotides
        dist_voulue = 3.3734740071740177

        # On définit l'erreur sur la distance, on introduit un terme en log pour éviter que les nucléotides se collent
        cond_dist = abs(res - dist_voulue) + 0.01*abs(np.log(res/dist_voulue))

        # On ajoute la condition sur les angles
        v1 = first-before_last
        v2 = second-last
        v = first-last
        original = rot_table[dinucleotide]

        # Différences

        # La première différence caractérise la fermeture circulaire de la chaîne
        dif1 = npl.norm(v1-v2)

        # La seconde différence caractérise l'écart entre l'original (résultat attendu) et le résultat obtenu
        dif2 = npl.norm(v[:3]-original)

        # On prend des poids qui permettent la convergence de l'algorithme
        # Il y a une grande importance de la distance et une grande importance de la fermeture circulaire
        # Le but est d'obtenir le score le plus faible possible
        return (6.0*cond_dist + 4*(2*dif1+dif2))

    def fit(self):
        """
        Si la fitness n'a pas été calculée, on la calcule. 
        Sinon on renvoie l'attribut fitn qui correspond a une fitness déjà calculée. 
        Fitnessbool est mis à jour en cas de mutation pour que la fitness soit recalculée. 
        """
        if self.fitnessbool == 0:
            a = self.fitness(self.gene, self.chaine)
            self.fitnessbool = 1
            self.fitn = a
            return a
        else:
            return self.fitn


def champ(pop, n):
    """
    Input: population de taille n 
    Return: Score du meilleur élément (champion) et de la moyenne des scores, le but est de minimiser le score
    """
    l_indice = list(pop.keys())
    res = np.inf
    moy = 0
    for k in l_indice:
        indiv = pop[k]
        score = indiv.fit()
        moy += score
        if score < res:
            res = score
    return [res, moy/n]

####
# TESTS
####


def test_individual():
    test = 'AATCGTAGCGACTGATTGCTAGTAGCT'
    monsieur = Individual(test)
    monsieur1 = monsieur.gene
    print(monsieur.fitness(monsieur1, test))
