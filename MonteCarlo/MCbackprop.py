####
# IMPORTATIONS
####

from cgi import test
from Monte_Carlo import *
from MCsimulation import *

####
# FONCTIONS
####


def backprop(noeud):
    """Input : noeud avec son score mis à jour par les simulations
    Output : None : mise à jour des scores jusqu'en haut"""
    current_node = noeud
    while current_node.parent != None:
        to_copy_node = current_node
        current_node = current_node.parent
        # On ajoute le score à chaque noeud
        current_node.score += to_copy_node.score
        current_node.nb_visite = to_copy_node.nb_visite + 1

####
# TESTS
####


def test_backprop():
    parent = Noeud({
        "AA": [35.62, 7.2], }, None, 0, 0, 0, [])
    node = Noeud(
        {"AA": [35.62, 7.2],
         "AC": [34.4, 1.10],
         },
        parent,
        100,
        5,
        1, [])

    assert backprop(node) == None

# test_backprop()
