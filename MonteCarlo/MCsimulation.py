####
# IMPORTATIONS
####

from Monte_Carlo import *
from MCexpansion import *

####
# FONCTIONS
####


def random_sequence(noeud, chaine):
    """Input : noeud à simuler jusqu'à arriver à une feuille
       Output : score associé à cette simulation aléatoire"""
    # On suppose l'existence d'une fonction fitness: Noeuds -> float, ici fitness doit être maximisée
    current_node = noeud
    while not check_terminal(current_node):
        i = np.random.randint(0, 100)
        j = np.random.randint(0, 100)
        current_node = genere_enfant(current_node, i, j)
    return fitness(current_node.gene, chaine)


def simulation(noeuds, chaine):
    """Input: liste des noeuds à simuler
     Output : None : meilleur score à remonter au noeud parent de ces noeuds"""
    scores = []
    for noeud in noeuds:
        scores.append((random_sequence(noeud, chaine), noeud))
    noeuds[0].parent.score, suivant = max(scores, key=lambda x: x[0])
    noeuds[0].parent.suivant.append(suivant)

####
# TESTS
####


test_chaine = 'CGATGCAT'


def test_random_sequence():
    parent = Noeud({
        "AA": [35.62, 7.2], }, None, 300, 4, 3, [])
    node = Noeud(
        {"AA": [35.62, 7.2],
         "AC": [34.4, 1.10],
         },
        parent,
        100,
        5,
        1, [])
    assert isinstance(random_sequence(node, test_chaine), float)


def test_simulation():
    parent = Noeud({
        "AA": [35.62, 7.2], }, None, 300, 4, 3, [])
    node = Noeud(
        {"AA": [35.62, 7.2],
         "AC": [34.4, 1.10],
         },
        parent,
        100,
        5,
        1, [])
    parent.suivant.append(node)
    exp = expansion(node)
    assert simulation(exp, test_chaine) == None

# test_random_sequence()
# test_simulation()
