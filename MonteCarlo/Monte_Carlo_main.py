####
# IMPORTATIONS
####

from RotTable import *
import numpy as np
from Monte_Carlo import *
from Monte_Carlo_selection import *
from MCbackprop import *
from MCexpansion import *
from MCsimulation import *
from Traj3D import Traj3D
from individual import transform

####
# FONCTIONS
####


def init_arbre(prof):
    global RACINE
    racine = RACINE
    noeud = selection(racine, 0, prof)
    return racine


def Monte_Carlo(DNA, n_iter, prec):
    """Input : DNA la chaine d'ADN, n_iter le nombre de boucles à faire dans Monte_Carlo, prec le nombre de fils à évaluer à chaque tour
    Return : La table de rotation du meilleur élément et l'affiche."""
    global RACINE, chaine
    racine = init_arbre(prec)
    # Boucle principale de l'algo: selection,expansion,...
    for k in range(n_iter):
        noeud = selection(racine, k, prec)
        if not noeud.suivant:
            liste = expansion(noeud)
        else:
            liste = [noeud]
        simulation(liste, DNA)
        backprop(noeud)

    # Exploration de l'arbre final pour trouver le meilleur
    champ = racine
    ChampPossible = champ.suivant
    while ChampPossible:
        champ = ChampPossible[0]
        score_max = champ.score
        for k in range(len(ChampPossible)):
            if ChampPossible[k].score > score_max:
                champ = ChampPossible[k]
                score_max = ChampPossible[k].score
                print(score_max)
        ChampPossible = champ.suivant
    # Si l'arbre construit n'est pas arrivé jusqu'à la fin, on sélectionne un chemin de meilleur score parmi d'autres aléatoires
    if 'TT' not in champ.gene:
        champ = selection_bis(champ, n_iter, 16-champ.next)
    # Affichage
    T = Traj3D()
    T.compute(DNA, RotTable(transform(champ.gene, DNA)))
    T.draw()
    return champ.gene


test1 = ''
file1 = open('plasmid_8k.txt', 'r')
file2 = open('plasmid_180k.txt', 'r')
test2 = ''
for x in file1:
    test1 += x.strip()
for x in file2:
    test2 += x.strip()

chaine = test1
#chaine = 'CGATCGAATCG'
Monte_Carlo(chaine, 100, 5)
