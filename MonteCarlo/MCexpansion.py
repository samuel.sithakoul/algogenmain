####
# IMPORTATIONS
####

from Monte_Carlo import *
import numpy as np

####
# FONCTIONS
####


def expansion(current_node):
    """Input : Noeud déjà exploré à partir duquel faire l'expansion
       Return : liste de nouveaux Noeuds à partir desquels s'étendre"""
    ix = np.random.randint(0, 1000, 10)
    iy = np.random.randint(0, 1000, 10)
    childs = []
    for i in ix:
        for j in iy:
            childs.append(genere_enfant(current_node, i, j))
    return childs

####
# TESTS
####


def test_expansion():
    parent = Noeud({
        "AA": [35.62, 7.2], }, None, 300, 4, 3, [])
    node = Noeud(
        {"AA": [35.62, 7.2],
         "AC": [34.4, 1.10],
         },
        parent,
        100,
        5,
        1, [])
    parent.suivant.append(node)
    exp = expansion(node)
    assert isinstance(exp[0], Noeud)

# test_expansion()
