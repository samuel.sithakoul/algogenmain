####
# IMPORTATIONS
####

from random import *
from Monte_Carlo import *
from Monte_Carlo import *
from RotTable import *
from MCUCB import *

####
# CONSTANTES
####

rot_table = {
    "AA": [35.62, 7.2, -154,      0.06,  0.6, 0],
    "AC": [34.4, 1.1,  143,      1.3,  5, 0],
    "AG": [27.7, 8.4,    2,      1.5,  3, 0],
    "AT": [31.5, 2.6,    0,      1.1,  2, 0],
    "CA": [34.5, 3.5,  -64,      0.9, 34, 0],
    "CC": [33.67, 2.1,  -57,      0.07,  2.1, 0],
    "CG": [29.8, 6.7,    0,      1.1,  1.5, 0],
    "CT": [27.7, 8.4,   -2,      1.5,  3, 0],
    "GA": [36.9, 5.3,  120,      0.9,  6, 0],
    "GC": [40, 5,  180,      1.2,  1.275, 0],
    "GG": [33.67, 2.1,   57,      0.07,  2.1, 0],
    "GT": [34.4, 1.1, -143,      1.3,  5, 0],
    "TA": [36, 0.9,    0,      1.1,  2, 0],
    "TC": [36.9, 5.3, -120,      0.9,  6, 0],
    "TG": [34.5, 3.5,   64,      0.9, 34, 0],
    "TT": [35.62, 7.2,  154,      0.06,  0.6, 0]
}

####
# FONCTIONS
####


def selection_iter(node, n_iter, bis=False):
    """Entree : un element de la classe noeud et n le nombre de fils que l'on veut
    # Sortie : la liste des n enfants selectionnes parmi ceux possibles"""
    l_ucb = []
    if bis:
        ix = np.random.randint(0, 1000, 100)
        jx = np.random.randint(0, 1000, 100)
        for i in ix:
            for j in jx:
                el = genere_enfant(node, i, j)
                node.suivant.append(el)

    for enf in node.suivant:
        ucb_enf = UCB(enf, n_iter)
        l_ucb.append((ucb_enf, enf))
    l_ucb = sorted(l_ucb, key=lambda x: x[0])
    enfant_select = l_ucb[-1][1]
    return enfant_select


def selection(node, n_iter, n):
    # Sortie : Le noeud issu des noeuds avec le meilleur score UCB après n itérations.
    enf = node
    for i in range(n):
        if enf.suivant != []:
            enf = selection_iter(enf, n_iter)
    return enf


def selection_bis(node, n_iter, n):
    # Sortie : Le noeud issu des noeuds avec le meilleur score UCB après n itérations.
    enf = node
    for i in range(n):
        enf = selection_iter(enf, n_iter, bis=True)
    return enf

####
# TESTS
####


def test_selection_iter():
    parent = Noeud({
    }, None, 0, 0, 0, [])
    node = Noeud(
        {"AA": [35.62, 7.2],
         },
        parent,
        100,
        5,
        1, [])
    parent.suivant = [node]
    assert isinstance(selection_iter(parent, 3), Noeud)


def test_selection():
    parent = Noeud({
    }, None, 0, 0, 0, [])
    node = Noeud(
        {"AA": [35.62, 7.2],
         },
        parent,
        100,
        5,
        1, [])
    parent.suivant = [node]
    assert isinstance(selection(parent, 3, 3), Noeud)


# test_selection_iter()
# test_selection()
