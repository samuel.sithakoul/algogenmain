####
# IMPORTATIONS
####

from RotTable import *
import numpy as np
from Traj3D import *
import numpy as np

####
# CONSTANTES
####

rot_table = {
    "AA": [35.62, 7.2, -154,      0.06,  0.6, 0],
    "AC": [34.4, 1.1,  143,      1.3,  5, 0],
    "AG": [27.7, 8.4,    2,      1.5,  3, 0],
    "AT": [31.5, 2.6,    0,      1.1,  2, 0],
    "CA": [34.5, 3.5,  -64,      0.9, 34, 0],
    "CC": [33.67, 2.1,  -57,      0.07,  2.1, 0],
    "CG": [29.8, 6.7,    0,      1.1,  1.5, 0],
    "CT": [27.7, 8.4,   -2,      1.5,  3, 0],
    "GA": [36.9, 5.3,  120,      0.9,  6, 0],
    "GC": [40, 5,  180,      1.2,  1.275, 0],
    "GG": [33.67, 2.1,   57,      0.07,  2.1, 0],
    "GT": [34.4, 1.1, -143,      1.3,  5, 0],
    "TA": [36, 0.9,    0,      1.1,  2, 0],
    "TC": [36.9, 5.3, -120,      0.9,  6, 0],
    "TG": [34.5, 3.5,   64,      0.9, 34, 0],
    "TT": [35.62, 7.2,  154,      0.06,  0.6, 0]
}
nucleotides = list(rot_table.keys())
chaine = "AAAGGATCTTCTTGAGATCCTTTTTTTCTGCGCGTAATCTGCTGCCAGTAAACGAAAAAACCGCCTGGGGAGGCGGTTTAGTCGAAGGTTAAGTCAG"
# print(rot_table)
All_Values = {}

####
# FONCTIONS ET CLASSES
####


def enfant_possible(codon, n):
    vect = rot_table[codon]
    x = vect[0]
    y = vect[1]
    dx = vect[3]
    dy = vect[4]
    Valeur_x = np.linspace(x-dx, x+dx, n)
    Valeur_y = np.linspace(y-dy, y+dy, n)
    Res = np.zeros((n, n, 2))
    for i, x in enumerate(Valeur_x):
        for j, y in enumerate(Valeur_y):
            Res[i][j][:] = [x, y]
    return Res


def create_All_Values(n):
    __ORIGINAL_ROT_TABLE = {
        "AA": [35.62, 7.2, -154,      0.06,  0.6, 0],
        "AC": [34.4, 1.1,  143,      1.3,  5, 0],
        "AG": [27.7, 8.4,    2,      1.5,  3, 0],
        "AT": [31.5, 2.6,    0,      1.1,  2, 0],
        "CA": [34.5, 3.5,  -64,      0.9, 34, 0],
        "CC": [33.67, 2.1,  -57,      0.07,  2.1, 0],
        "CG": [29.8, 6.7,    0,      1.1,  1.5, 0],
        "CT": [27.7, 8.4,   -2,      1.5,  3, 0],
        "GA": [36.9, 5.3,  120,      0.9,  6, 0],
        "GC": [40, 5,  180,      1.2,  1.275, 0],
        "GG": [33.67, 2.1,   57,      0.07,  2.1, 0],
        "GT": [34.4, 1.1, -143,      1.3,  5, 0],
        "TA": [36, 0.9,    0,      1.1,  2, 0],
        "TC": [36.9, 5.3, -120,      0.9,  6, 0],
        "TG": [34.5, 3.5,   64,      0.9, 34, 0],
        "TT": [35.62, 7.2,  154,      0.06,  0.6, 0]
    }
    l_codon = list(__ORIGINAL_ROT_TABLE.keys())
    for codon in l_codon:
        All_Values[codon] = enfant_possible(codon, n)


create_All_Values(1000)
# Les autres fonctions


def check_terminal(feuille):
    # Les feuilles sont representees par des chaines de caracteres non abouties
    if feuille.next == 16:
        return True
    return False


def fitness(gene, chaine):
    """Input:gêne aléatoire et chaîne dont il est issu
    Return: fitness calculée sur ce gêne défini par la distance euclidienne entre les extrémités"""
    new_rot_table = RotTable(
        gene)  # Table de rotation particulière à ce gêne
    traj = Traj3D()
    traj.compute(chaine, new_rot_table)
    rot_table2 = new_rot_table.get_table()
    # Trajet sous la forme [Vect(a,b,c,1),...,Vect(a,b,c,1)]
    path = traj.getTraj()
    first = np.asarray(path[0])
    last = np.asarray(path[-1])
    second = np.asarray(path[1])
    before_last = np.asarray(path[-2])
    res = np.linalg.norm(last-first)
    dinucleotide = chaine[-1]+chaine[0]
    second = np.asarray(path[1])
    # En pratique, on observe cette distance entre deux nucléotides
    dist_voulue = 3.3734740071740177

    # On définit l'erreur sur la distance, on introduit un terme en log pour éviter que les nucléotides se collent
    cond_dist = abs(res - dist_voulue) + 0.01*abs(np.log(res/dist_voulue))

    # On ajoute la condition sur les angles
    v1 = first-before_last
    v2 = second-last
    v = first-last
    original = rot_table2[dinucleotide]

    # Différences

    # La première différence caractérise la fermeture circulaire de la chaîne
    dif1 = np.linalg.norm(v1-v2)

    # La seconde différence caractérise l'écart entre l'original (résultat attendu) et le résultat obtenu
    dif2 = np.linalg.norm(v[:2]-original)

    # On prend des poids qui permettent la convergence de l'algorithme
    # Il y a une grande importance de la distance et une grande importance de la fermeture circulaire
    # Le but est d'obtenir le score le plus faible possible
    return 1/(6.0*cond_dist + 4*(2*dif1+dif2))


def evaluate_terminal(feuille, chaine):
    return fitness(feuille["gene"], chaine)

# parent={"gene":[(x,y),(w,z)...], "next":i,"score":}


def genere_enfant(parent, i, j):
    """
    Construction d'un dictionnaire enfant qui implemente le prochain dinucleotide
    Entree: dictionnaire parent contenant un debut de gene et l'indice du prochain dinucleotide a considerer (next) ainsi que le score
    Sortie: dictionnaire enfant contenant les memes informations + un dinucleotide supplementaire  + score
    """
    enfant = Noeud({}, None, 0, 0, 0, [])
    # On trouve le prochain nucleotide en fonction de celui du parent
    dinucleotide_suivant = nucleotides[parent.next]
    # On choisit des valeurs aleatoires dans le bon intervalle
    x, y = All_Values[dinucleotide_suivant][i][j][0], All_Values[dinucleotide_suivant][i][j][1]
    # on ajoute a l'enfant les genes du parent et le nouveau gene
    enfant.gene = parent.gene.copy()
    enfant.gene[dinucleotide_suivant] = [x, y]
    # On incremente
    enfant.next = parent.next+1
    enfant.parent = parent
    enfant.score = 0
    enfant.nb_visite = 0

    return enfant


class Noeud:
    def __init__(self, gene, parent, score, nb_visite, next, suivant):
        self.gene = gene
        self.parent = parent
        self.score = score
        self.nb_visite = nb_visite
        self.next = next
        self.suivant = suivant


RACINE = Noeud({}, None, 0, 0, 0, [])
ix = np.random.randint(0, 1000, 100)
jx = np.random.randint(0, 1000, 100)
for i in ix:
    for j in jx:
        RACINE.suivant.append(genere_enfant(RACINE, i, j))
