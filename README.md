# ST2 (Théorie des Jeux) - EI Algorithmique Génétique et Monte Carlo Tree Search

## Contexte
Toutes les cellules qui constituent la vie sur Terre comportent en elles une ou plusieurs molécules d'ADN qui sont le support de l'information génétique. Ces molécules, plus ou moins longues, sont composées d'une succession de nucléotides (ou bases : A, C, G et T) qui interagissent avec de nombreux éléments cellulaires et dont le positionnement dans l'espace joue un rôle important dans l'adaptation de la cellule à son environnement (chaleur, famine, stress...). Si les séquences d'ADN sont aujourd'hui très largement étudiées à travers leur séquence textuelle (succession de A, C, G et T), il est très instructif de les étudier à partir de leur trajectoire tri-dimensionnelle. En 1993, des biopysiciens ont établi un modèle de conformation 3D qui permet de transformer une suite de nucléotides (sous forme de lettres) en une trajectoire tri-dimensionnelle. Dès lors, il est possible de représenter toute séquence textuelle d'ADN en une trajectoire 3D.

## Problématique
Ce modèle ayant été développé pour de courtes séquences d'ADN nu, il ne prend pas en compte toutes les caractéristiques d'une longue chaîne au sein de la cellule (surenroulements, nucléosomes, interactions longue distance...). Par exemple, si on observe un chromosome bactérien (longue séquence d'ADN constituant une bactérie) ou un plasmide (petite séquence présente au sein des bactéries), on s'aperçoit que ce chromosome ou ce plasmide est circulaire, i.e. les deux extrémités ont été "collées" l'une à l'autre. Le modèle pré-cité ne rend pas compte de ce phénomène lorsque l'on représente la trajectoire 3D d'un chromosome bactérien ou d'un plasmide.

## Énoncé
L'objectif de ce projet est de modifier le modèle de conformation 3D donné afin de rendre un plasmide circulaire. Pour cela, un algorithme génétique sera développé en Python et structuré en classes (programmation orientée objet).

Le projet est à réaliser en groupes de maximum 4 personnes (imposé par l'école).

## Méthode de résolution à l'aide d'algorithmes génétiques
Les algorithmes génétiques utilisées ici vont permettre d'optimiser la valeur des différents paramètres pour chaque type de 
dinucléotide afin d'arriver à replier un plasmide au mieux. L'intervalle de recherche pour chaque paramètre d'un nucléotide donné
est défini parla valeur de référence avec plus ou moins l'écart type. On cherche alors à minimiser la distance euclidienne entre
le premier dinucléotide et le dernier pour avoir un repliement, modulé par une pénalité due au non parallélisme de ces nucléotides
(un bout de chaîne arrivant orthogonalement pour se replier est peu envisageable). Les symétries entre certains dinucléotides ont 
été pris en compte pour ajuster la valeur des paramètres symétriquement lors de l'optimisation. La fonction fitness définit le score
de l'individu et est à minimiser. 
L'algorithme se divise en 3 étapes:
- la sélection : On effectue une sélection par tounoi dans laquelle on choisit aléatoirement des individus qui s'affrontent et l'individu avec le plus petit score gagne. On obtient une population de taille réduite. L'objectif est de garder les bons individus en s'assurant
que l'on garde également des individus moins bons. 
- le croisement : On effectue des croisements entres les individus pour retrouver la taille de la population intiale. On effectue un croisement en 4 points. On construit un fils avec une partie des chromosomes des deux parents (avec alternance selon les points de découpe)
- la mutation : Les individus mutent avec une certaine probabilité. On définit deux types de mutation : une qui ne fait muter qu'un codon,
la seconde réinitialise le code génétique (on gènère une nouvelle table de rotation). La seconde mutation est très rare. Pour les deux 
mutations, on s'assure que les nouveaux paramètres restent dans l'intervalle autorisé. La mutation 1 permet d'accélérer la convergence vers un bon individu et la mutation 2 permet un renouvellement dans le cas des grosses populations. 
Les fonctions pré-implémentées dans Main, Traj3D et RotTable ont permi notamment de calculer l'ensemble des positions à partir 
d'une table de rotation représentant un gène donné.

## Méthode de résolution à l'aide de la méthode Monte Carlo Tree Search
La méthode MCTS part du même objectif que les algorithmes génétiques : on utilise l'inverse de la fonction de fitness que l'on
cherche donc à maximiser ici. Chaque niveau de l'arbre de recherche correspond à un dinucléotide parmi les 16 possibles dont
on cherche à optimiser les paramètres. Les branches correspondent aux différentes valeurs possibles dans chaque intervalle de 
recherche pour le dinucléotide du noeud suivant; intervalle ici discret pour rester sur un arbre fini. La racine n'a pas de valeur significative à part de remonter le score global.
L'algorithme se divise en 4 étapes:
- la sélection : on se base sur l'arbre déjà construit et on descend de branche en branche en choisissant celle avec le plus de potentiel, c'est-à-dire avec l'UCB (Upper Confidence Bound) la plus élevée. On s'arrête lorsque l'on arrive à une certaine profondeur donnée en entrée de l'algorithme.
- l'expansion : si le noeud sélectionné n'a pas de fils. On en crée un certain nombre dont les valeurs des branches sont aléatoires dans les intervalles de recherche. Sinon il s'agit juste du noeud de la sélection.
- la simulation: on simule de manière aléatoire l'affectation de valeurs de paramètres pour chaque branche issue de chaque noeud né de l'expansion jusqu'à arriver à une feuille. On évalue alors le score de chaque feuille avec la fonction de fitness et on conserve uniquement la feuille avec le meilleur score. Ce score est remonté au noeud de l'expansion.
- la backpropagation : on ajoute le score issu de la simulation du noeud de l'expansion jusqu'à la racine de proche en proche et on incrémente le nombre de visite de chaque noeud parcouru.
L'algorithme se termine pour un certain nombre d'itérations et on obtient le gène en parcourant l'arbre construit depuis la racine en sélectionnant à chaque fois la branche de meilleur score.

## Comment lancer le projet
- Pour l'algorithme génétique, le fichier principal est Jeu.py. Il suffit de modifer les paramètres d'entrée à la fin du script et de spécifier la chaîne à analyser puis de lancer le scipt dans le dossier AlgoGen
- Pour MCTS, il suffit de modifier aussi les paramètres d'entrée de Monte_Carlo_main.py et de lancer le script dans le dossier MCTS
## Ressources

Fonctions d'utilitaire déjà fournies par les encadrants:
- le fichier <tt>Traj3D.py</tt> implémentant le moteur de calcul d'une trajectoire 3D,
- le fichier <tt>Rot_Table.py</tt> contenant la table de rotations nécessaires au calcul d'une trajectoire 3D (ce fichier devra être modifié),
- le fichier <tt>Main.py</tt> illustrant un exemple d'utilisation de la classe Traj3D,
- deux fichiers <tt>.fasta</tt> contenant les séquences de deux plasmides de longueur différente (8 000 dans un cas et 180 000 dans l'autre).

## Licence
Ce projet est sous la licence TeamAlgoGen. 