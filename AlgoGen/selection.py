####
# IMPORTATIONS
####

from RotTable import *
from Traj3D import *
from individual import *
from individual import Individual
import numpy as np

####
# FONCTIONS
####


def selection_tournoi(jeux):
    """ 
    Input : une instance de jeu contenant la population d'individus de classe "Individu" et la taille de la population 
    Return : la population des individus sélectionnés selon la méthode du tournoi 
    """
    pop = jeux.pop
    n = jeux.data_resol['taille_pop']

    # On définit le nombre de matchs dans le tournoi
    n_choisi = int(n*jeux.data_resol['rapport_choisi'])
    individual_keys = list(pop.keys())

    for i in range(n-n_choisi):

        # On choisit deux individus aleatoires, defini par l'indice de leur clée dans la liste individual_keys
        x = np.random.randint(0, n-1-i)
        y = np.random.randint(0, n-1-i)

        # Un individu ne peut pas s'affronter lui-meme
        while x == y:
            y = np.random.randint(0, n-1-i)

        # On compare le score et on supprime l'individu dont le score est le plus faible
        indx, indy = pop[individual_keys[x]], pop[individual_keys[y]]
        score1 = indx.fit()
        score2 = indy.fit()

        if score1 > score2:
            del pop[individual_keys[x]]
            individual_keys.pop(x)
        else:
            del pop[individual_keys[y]]
            individual_keys.pop(y)

####
# TESTS
####


def test_selection(m):
    """
    Permet d'observer les données en entrée puis en sortie de la fonction sélection 
    """
    pop = {}
    chaine = "AAAGGATCTTCTTGAGATCCTTTTTTTCTGCGCGTAATCTGCTGCCAGTAAACGAAAAAACCGCCTGGGGAGGCGGTTTAGTCGAAGGTTAAGTCAG"
    for i in range(m):
        indiv = Individual(chaine)
        pop[str(i)] = indiv
    print("Population initiale : "+"\n")
    for i in range(len(pop)):
        print(pop[str(i)].score)
    print("\n" + "population apres selection : "+"\n")
    pop = selection_tournoi(pop, m)
    l = list(pop.keys())
    for key in l:
        print(pop[key].fitness(pop[key].get_gene(), pop[key].get_chaine()))
