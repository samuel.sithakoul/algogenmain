####
# IMPORTATIONS
####

import random as rd
from individual import *

####
# FONCTIONS
####


def difference(l, n):
    """
    Les clées de la population sont des chaines de caractères d'entiers
    """
    return set([str(k) for k in range(n)]).difference(set(l))


def croisement(jeux):
    """
    Input : une instance de jeu contenant la population après sélection (Pop)
    Return: la nouvelle population issue du croisement.
    Il s'agit du croisement en 4 points.
    """
    Pop = jeux.pop
    n = jeux.data_resol['taille_pop']
    clef = list(Pop.keys())
    liste_indice = difference(clef, n)

    for crois in liste_indice:

        # On choisit aléatoirement les parents
        indiceParent1 = rd.choice(clef)
        indiceParent2 = rd.choice(clef)
        parent1 = Pop[indiceParent1]
        parent2 = Pop[indiceParent2]
        ListIndice = list(table.keys())

        # On construit le fils
        RotParent1 = parent1.get_gene()
        RotParent2 = parent2.get_gene()
        RotFils = {
            "AA": [35.62, 7.2],
            "AC": [34.4, 1.1],
            "AG": [27.7, 8.4],
            "AT": [31.5, 2.6],
            "CA": [34.5, 3.5],
            "CC": [33.67, 2.1],
            "CG": [29.8, 6.7],
            "CT": [27.7, 8.4],
            "GA": [36.9, 5.3],
            "GC": [40, 5],
            "GG": [33.67, 2.1],
            "GT": [34.4, 1.1],
            "TA": [36, 0.9],
            "TC": [36.9, 5.3],
            "TG": [34.5, 3.5],
            "TT": [35.62, 7.2]
        }
        # On choisit ensuite 4 indices sur lesquels on va couper les matrices de rotation et réaliser un croisement en quatre points.
        # Ces quatres indices peuvent éventuellement être égaux auquel cas on réalise un croisement sur moins de points.
        Ind = [rd.randint(1, 15), rd.randint(1, 15),
               rd.randint(1, 15), rd.randint(1, 15)]
        Indsort = sorted(Ind)
        for count, cle in enumerate(ListIndice):

            if count < Indsort[0]:
                RotFils[cle] = RotParent1[cle].copy()
            elif count < Indsort[1]:
                RotFils[cle] = RotParent2[cle].copy()
            elif count < Indsort[2]:
                RotFils[cle] = RotParent1[cle].copy()
            elif count < Indsort[3]:
                RotFils[cle] = RotParent2[cle].copy()
            else:
                RotFils[cle] = RotParent1[cle].copy()

        # Respect des symétries
        RotFils["TT"] = RotFils["AA"]
        RotFils["GT"] = RotFils["AC"]
        RotFils["CT"] = RotFils["AG"]
        RotFils["CA"] = RotFils["TG"]
        RotFils["CC"] = RotFils["GG"]
        RotFils["GA"] = RotFils["TC"]
        fils = Individual(parent1.get_chaine())
        fils.gene = RotFils

        # On ajoute le fils à PopSortie
        Pop[crois] = fils
