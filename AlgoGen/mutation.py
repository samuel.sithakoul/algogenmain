####
# IMPORTATIONS
####

from individual import *

####
# FONCTIONS
####


def mutation(jeux):
    """
    Chaque individu a une chance de muter.
    Si l'individu mute, ajoute un petit nombre à une ligne de sa matrice de rotation en conservant la symétrie.
    Input : une instance de jeu contenant le dictionnaire de population, la probabilité pour la première mutation
    Return : Pop la population mutée
    """
    Pop = jeux.pop
    L = Pop.keys()
    p = jeux.data_resol['proba_mut1']
    # La liste Ldoublet va permettre de conserver la symétrie
    Ldoublet = [["AT"], ["CG"], ["TA"], ["GC"], ["AA", "TT"], [
        "AC", "GT"], ["AG", "CT"], ["CA", "TG"], ["CC", "GG"], ["GA", "TC"]]
    ndoublet = len(Ldoublet)
    for k in L:  # Boucle sur les clés du dictionnaire Pop
        if np.random.random() < p:  # Si l'individu mute
            # On choisit une de ses lignes aléatoirement
            i = np.random.randint(ndoublet)
            doublet = Ldoublet[i]
            # On ajoute à cette ligne un nombre aléatoire contenu entre +/- l'écart type
            e1 = np.random.choice([-1, 1])
            e2 = np.random.choice([-1, 1])
            a = 0.5*(e1*Pop[k].gene[doublet[0]][0]-e1*Individual.doublets[doublet[0]]
                     [0]+Individual.ecart_types[doublet[0]][0])
            b = 0.5*(e1*Pop[k].gene[doublet[0]][1]-e1*Individual.doublets[doublet[0]]
                     [1]+Individual.ecart_types[doublet[0]][1])
            Pop[k].gene[doublet[0]][0] += 2*a*np.random.random_sample()-a
            Pop[k].gene[doublet[0]][1] += 2*b*np.random.random_sample()-b
            Pop[k].fitnessbool = 0
            if len(doublet) > 1:
                Pop[k].gene[doublet[1]][0] = Pop[k].gene[doublet[0]][0]
                Pop[k].gene[doublet[1]][1] = Pop[k].gene[doublet[0]][1]
    return Pop


def mutation2(jeux):
    """
    Chaque individu a une chance de muter.
    Si l'individu mute, on change tous ses paramaètres
    Input : Une instance de jeu contenant le dictionnaire de population, la probabilité pour la seconde mutation
    Return : Pop la population mutée
    """
    Pop = jeux.pop
    L = Pop.keys()
    p = jeux.data_resol['proba_mut2']
    for k in L:  # Boucle sur les clés du dictionnaire Pop
        # Pour chaque individu qui mute on change ses gênes aléatoirement en suivant la loi uniforme sur l'intervalle donné par la chaine
        if np.random.random_sample() < p:
            gene = Pop[k].random_dico()
            Pop[k].gene = gene
            Pop[k].fitnessbool = 0
    return Pop

####
# TESTS
####


def test_mutation():
    indiv_test = Individual('AATCGTAGCGACTGATTGCTAGTAGCT')
    print("Avant mutation:")
    print(indiv_test.gene)
    print("Après mutation:")
    mutation({1: indiv_test})
    print(indiv_test.gene)
