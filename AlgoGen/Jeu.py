####
# IMPORTATIONS
####

from individual import *
from Main import *
from selection import *
from croisement import *
from mutation import *
import numpy as np

####
# FONCTIONS ET CLASSES
####


class jeu:

    def __init__(self, individu, liste_etape, representation, data_resol):
        self.letape = liste_etape
        self.individu = individu
        self.represent = representation
        self.data_resol = data_resol
        self.bool_jeu = 0
        self.pop = data_resol['pop']
        self.data = []

    def tour(self):
        """
        Fonction qui réalise le passage de la k ième population à la k+1 ième
        """

        # On parcourt la liste des fonctions appliqué dans l'ordre
        for funct in self.letape:
            # On fait évoluer la population
            funct(self)
            # Ci dessous, voici des fonctions qui nous permettent de tester le code.
            # print('etape' + str(len([x.get_gene()])))
            # for x in pop_etape.values()])))
            # print(len(pop_etape))

    def lancer(self):
        '''
        Lance une partie et stocke les résultats.
        '''
        for i in range(self.data_resol['nb_iteration']):
            self.tour()  # Fait un tour
            # Indique où on en est dans la boucle
            print("etape"+str(i+1)+"/"+str(self.data_resol['nb_iteration']))
            # Enregistre la fitness du meilleur et la fitness moyenne de la pop
            data_tour = champ(self.pop, self.data_resol['taille_pop'])
            # On représente en temps réelle les résultats de chaque génération
            print('La moyenne est :')
            print(data_tour[1])
            print('\n')
            print('Le champion est :')
            print(data_tour[0])
            print('\n')
            # Stocke le meilleur et la moyenne de chaque génération
            self.data.append(data_tour)
        # On stocke le fait qu'un jeu a été jouer (cela nous permettra dans la représentation de savoir si il faut lancer un jeu ou pas)
        self.bool_jeu = 1

    def representation(self):
        """
        Fonction permettant de representer un jeu définit
        """
        # Si aucun jeu n'a été joué
        if self.bool_jeu == 0:
            # On lance un jeu
            self.lancer()
            # On le représente
            self.represent(self.pop, self.data,
                           self.data_resol['nb_iteration'])
        # SI il a déjà été joué
        else:
            # On le représente
            self.represent(self.pop, self.data,
                           self.data_resol['nb_iteration'])


def function_3D(Pop, data, nb_iteration):
    """
    Fonction représentant le résultat d'un jeu
    """
    l_indice = list(Pop.keys())
    elt = Pop[l_indice[0]]
    DNA = elt.chaine
    T = Traj3D()
    # Calcul du champion final
    res = np.inf
    champion = elt
    for k in l_indice:
        indiv = Pop[k]
        score = indiv.fitness(indiv.get_gene(), indiv.get_chaine())
        if score < res:
            champion = indiv
            res = score
    # On trouve l'ensemble des moyennes et meilleurs fitness à chaque génération
    Moy = []
    Best = []
    for k in range(nb_iteration):
        Moy.append(data[k][0])
        Best.append(data[k][1])
    # On représente le résultat de l'évolution de la fitness sur un graphique
    plt.plot(Moy)
    plt.plot(Best)
    # Enregistrer le graphique
    plt.savefig('save/fig_evol.png')
    # On le montre
    plt.show()
    plt.clf()
    plt.plot(Best)
    plt.yscale('log')
    plt.savefig('save/fig_evol_log.png')
    plt.show()
    # On représente le résultat de la forme 3D de notre ADN
    T.compute(DNA, RotTable(transform(champion.get_gene(), DNA)))
    print(champion.fitness(champion.gene, champion.chaine))
    path = T.getTraj()
    # On va calculer la distance entre la 1er et la dernière nucléotide
    first = np.asarray(path[0])
    last = np.asarray(path[-1])
    dist_voulue = 3.3734740071740177
    res = np.linalg.norm(last-first)
    # On affiche la distance
    print("\n"+"la distance finale est :")
    print(abs(res - dist_voulue))
    # On affiche la molécule
    T.draw()


test1 = ''
file1 = open('plasmid_8k.txt', 'r')
file2 = open('plasmid_180k.txt', 'r')
test2 = ''
for x in file1:
    test1 += x.strip()
for x in file2:
    test2 += x.strip()


####
# EXEMPLES
####

# Liste des étapes appliquer pour transformer la k ieme
liste_etape = [selection_tournoi, croisement, mutation,
               mutation2]


def fun_test_5(taille_1er_pop):
    """
    Faire 5 jeux avec une taille_initiale puis fusionner les pop et relancer un jeu
    """

    # Initialisation des parametres
    parametre = {'nb_iteration': 100, 'taille_pop': taille_1er_pop,
                 'chaine': test1, 'rapport_choisi': 1/10, 'proba_mut1': 1, 'proba_mut2': 0.0005}

    parametre['pop'] = {str(k): Individual(parametre['chaine'])
                        for k in range(parametre['taille_pop'])}
    # Initialisation de la population.
    jeux1 = jeu(Individual, liste_etape, function_3D,
                parametre)  # Définition du 1er jeu
    parametre['pop'] = {str(k): Individual(parametre['chaine'])
                        for k in range(parametre['taille_pop'])}
    # Joue le 1er jeu
    jeux1.lancer()
    # Définition du 2er jeu
    jeux2 = jeu(Individual, liste_etape, function_3D,
                parametre)
    parametre['pop'] = {str(k): Individual(parametre['chaine'])
                        for k in range(parametre['taille_pop'])}
    # Joue le 2er jeu
    jeux2.lancer()
    # Définition du 3er jeu
    jeux3 = jeu(Individual, liste_etape, function_3D,
                parametre)
    parametre['pop'] = {str(k): Individual(parametre['chaine'])
                        for k in range(parametre['taille_pop'])}
    # Joue le 3er jeu
    jeux3.lancer()
    # Définition du 4er jeu
    jeux4 = jeu(Individual, liste_etape, function_3D,
                parametre)
    parametre['pop'] = {str(k): Individual(parametre['chaine'])
                        for k in range(parametre['taille_pop'])}
    # Joue le 4er jeu
    jeux4.lancer()
    # Définition du 5er jeu
    jeux5 = jeu(Individual, liste_etape, function_3D,
                parametre)  # Définition du 1er jeu
    parametre['pop'] = {str(k): Individual(parametre['chaine'])
                        for k in range(parametre['taille_pop'])}
    # Joue le 5er jeu
    jeux5.lancer()

    # On pointe les population résultats des 5 premiers jeux
    pop6 = {}
    pop1 = jeux1.pop
    pop2 = jeux2.pop
    pop3 = jeux3.pop
    pop4 = jeux4.pop
    pop5 = jeux5.pop
    # Compte le nombre d'élément dans pop6
    i = 0
    # On va ajouter les éléments de chaque population des jeux 1,2,3,4 et 5 à la population de départ du 6eme jeu
    for elt in pop1:
        pop6[str(i)] = pop1[elt]
        i += 1
    for elt in pop2:
        pop6[str(i)] = pop2[elt]
        i += 1
    for elt in pop3:
        pop6[str(i)] = pop3[elt]
        i += 1
    for elt in pop4:
        pop6[str(i)] = pop4[elt]
        i += 1
    for elt in pop5:
        pop6[str(i)] = pop5[elt]
        i += 1
    parametre['pop'] = pop6
    parametre['taille_pop'] = 5*taille_1er_pop
    parametre['rapport_choisi'] = 1/4
    parametre['proba_mut1'] = 0.3
    # Définition du jeu avec fusion des 5 premieres population
    jeux6 = jeu(Individual, liste_etape, function_3D, parametre)
    jeux6.representation()


def fun_test_1():
    '''
    Lance un jeu avec les parametres ci-dessous
    '''
    parametre = {'nb_iteration': 100, 'taille_pop': 1000,
                 'chaine': test1, 'rapport_choisi': 1/4, 'proba_mut1': 0.3, 'proba_mut2': 0.0005}
    parametre['pop'] = {str(k): Individual(parametre['chaine'])
                        for k in range(parametre['taille_pop'])}
    parametre['taille_pop'] = 50
    # On définit le jeu
    jeux = jeu(Individual, liste_etape, function_3D, parametre)
    # On le lance et on affiche les résultats
    jeux.representation()


# On lance une simulation
fun_test_1()
